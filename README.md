# travelport-locomote-flight-search-app

This is a sample flight search application using React, Node.js, Express and Webpack. It is also configured with webpack-dev-server, eslint, prettier and babel.

## Introduction

This is a simple full stack [React](https://reactjs.org/) application with a [Node.js](https://nodejs.org/en/) and [Express](https://expressjs.com/) backend. Client side code is written in React and the backend API is written using Express. This application is configured with [Airbnb's ESLint rules](https://github.com/airbnb/javascript) and formatted through [prettier](https://prettier.io/).

Unit Tests are implemented using Jest, Enzyme.

### Development mode

In the development mode, we will have 2 servers running. The front end code will be served by the [webpack dev server](https://webpack.js.org/configuration/dev-server/) which helps with hot and live reloading. The server side Express code will be served by a node server using [nodemon](https://nodemon.io/) which helps in automatically restarting the server whenever server side code changes.

### Production mode

In the production mode, we will have only 1 server running. All the client side code will be bundled into static files using webpack and it will be served by the Node.js/Express application.

## Quick Start

```bash
# Clone git repository
git clone https://prashant_alhat@bitbucket.org/prashant_alhat/travelport-locomote-flight-search-app.git
 
# Go inside the directory
cd travelport-locomote-flight-search-app

# Install dependencies
yarn (or npm install)

# Start development server
yarn dev (or npm run dev)

# Build for production
yarn build (or npm run build)

# Start production server
yarn start (or npm start)

# Linting
npm run lint

# Linting Fix
npm run lint:fix

# Test
npm run test

# Test Watch
npm run test:watch

```
