module.exports = {
    formatMinutestoDurationString(minutes) {
        var hours = Math.floor(minutes / 60);
        var minutes = minutes % 60;
        return `${hours} hr ${minutes} mins`;
    },
    sort(items, property, direction) {
        function compare(a, b) {
            if (!a[property] && !b[property]) {
                return 0;
            } else if (a[property] && !b[property]) {
                return -1;
            } else if (!a[property] && b[property]) {
                return 1;
            } else {
                const value1 = a[property].toString().toUpperCase(); // ignore upper and lowercase
                const value2 = b[property].toString().toUpperCase(); // ignore upper and lowercase
                if (value1 < value2) {
                    return direction === 0 ? -1 : 1;
                } else if (value1 > value2) {
                    return direction === 0 ? 1 : -1;
                } else {
                    return 0;
                }
            }
        }
        return items.sort(compare);
    }
}