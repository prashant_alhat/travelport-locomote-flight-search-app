const express = require('express');
const os = require('os');
const mcache = require('memory-cache');
const fetch = require('node-fetch');

const app = express();
const config = require('./config');
const helper = require('./helper');

app.use(express.static('dist'));

// duration in seconds
var cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url
        let cachedBody = mcache.get(key)
        if (cachedBody) {
            res.send(cachedBody)
            return
        } else {
            res.sendResponse = res.send
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body)
            }
            next()
        }
    }
}

app.get('/api/airlines', cache(3600), async (req, res) => {
    try {
        const response = await fetch(`${config.LocomoteAPIBaseURL}airlines`);
        const some = await response.json();
        res.send(some);
    } catch (error) {
        console.log(error);
    }
})

app.get('/api/airports/:search', cache(3600), async (req, res) => {
    try {
        const { search } = req.params;
        const response = await fetch(`${config.LocomoteAPIBaseURL}airports?q=${search}`);
        res.send(await response.json());
    } catch (error) {
        console.log(error);
    }
})

app.get('/api/flight_search/:date/:from/:to/:sortBy/:sortDirection', cache(60), async (req, res) => {
    try {
        let { date, from, to, sortyBy, sortDirection } = req.params;
        console.log(req.params);
        if (!sortyBy) {
            sortyBy = 'Price'
        }
        if (!sortDirection) {
            sortDirection: 'Asc'
        }
        const response = await fetch(`${config.LocomoteAPIBaseURL}flight_search/QF?date=${date}&from=${from}&to=${to}`);
        const searchResults = await response.json();
        let formattedReturn = [];
        // format output
        if (searchResults && searchResults.length > 0) {
            for (let i = 0; i < searchResults.length; i++) {
                const result = searchResults[i];
                const { airline, start, finish } = result;
                formattedReturn.push(
                    {
                        Airline: {
                            Name: airline.name,
                            Code: airline.code
                        },
                        Start: {
                            DateTime: start.dateTime,
                            AirportCode: start.airportCode,
                            AirportName: start.airportName,
                            CityName: start.cityName,
                            CityCode: start.cityCode
                        },
                        Finish: {
                            DateTime: finish.dateTime,
                            AirportCode: finish.airportCode,
                            AirportName: finish.airportName,
                            CityName: finish.cityName,
                            CityCode: finish.cityCode
                        },
                        FlightNumber: result.flightNum,
                        Duration: helper.formatMinutestoDurationString(result.durationMin),
                        Distance: result.distance,
                        FormattedPrice: `${result.price} AUD`,
                        Price: result.price,
                    }
                );
            }
        }
        const sortedOutput = helper.sort(formattedReturn, sortyBy, sortDirection == 'Asc' ? 0 : 1);
        res.send(sortedOutput);
    } catch (error) {
        console.log(error);
    }
})

app.listen(8080, () => console.log('Listening on port 8080!'));


/**
 * Sort by 
 * 1. Travel Time
 * 2. Price
 */