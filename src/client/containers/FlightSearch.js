import React from 'react';
import moment from 'moment';

import SearchPanelForm from '../components/SearchPanel';
import FlightResults from '../components/FlightResults';

import { fetchFlights } from '../services';

const searchQueryDateFormat = 'YYYY-MM-DD';
const defaultFromDate = moment();

class FlightSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fromCity: '',
      toCity: '',
      date: defaultFromDate.toDate(),
      data: [],
      loading: false,
      sortField: 'price',
      sortOrder: 'asc'
    };
  }

  onFromAirportChanged = (selection) => {
    this.setState({
      fromCity: selection.key
    });
  }

  onToAirportChanged = (selection) => {
    this.setState({
      toCity: selection.key
    });
  }

  onCalendarChange = (selection) => {
    this.setState({
      date: selection[0].toDate()
    });
  }

  changeFilterColumn = (selection) => {
    this.setState({
      date: selection[0].toDate()
    });

    this.searchFlights();
  }

  changeFilterDirection = (selection) => {
    this.setState({
      date: selection[0].toDate()
    });

    this.searchFlights();
  }

  searchFlights = async () => {
    this.setState({
      loading: true
    });
    const flightDate = moment(this.state.date).format(searchQueryDateFormat);
    const flightData = await fetchFlights(flightDate, this.state.fromCity, this.state.toCity,'Price', 'Asc');
    this.setState({
      data: flightData,
      loading: false
    });
  }

  render() {
    return (
      <div className="search-area">
        <SearchPanelForm
          searchFlights={this.searchFlights}
          onCalendarChange={this.onCalendarChange}
          onFromAirportChanged={this.onFromAirportChanged}
          onToAirportChanged={this.onToAirportChanged}
          fetchingResults={this.state.loading}
        />
        <FlightResults searchResults={this.state.data} />
      </div>
    );
  }
}

export default FlightSearch;
