import React from 'react';
import PropTypes from 'prop-types';
import { Select, Spin } from 'antd';
import debounce from 'lodash/debounce';

import { fetchAirports } from '../services';

const Option = Select.Option;
class AirportSearch extends React.Component {
  constructor(props) {
    super(props);
    this.lastFetchId = 0;
    this.findAirports = debounce(this.findAirports, 800);
    this.state = {
      data: [],
      value: [],
      fetching: false,
    };
  }


    findAirports = async (searchQuery) => {
      this.setState({ data: [], fetching: true });
      const data = await fetchAirports(searchQuery);
      this.setState({ data, fetching: false });
    }

    handleChange = (value) => {
      this.setState({
        value,
        data: [],
        fetching: false,
      });

      this.props.onChange(value);
    }

    render() {
      const { fetching, data, value } = this.state;
      return (
        <Select
          showSearch
          labelInValue
          value={value}
          placeholder={this.props.placeHolder}
          notFoundContent={fetching ? <Spin size="small" /> : null}
          filterOption={false}
          onSearch={this.findAirports}
          onChange={this.handleChange}
          style={{ width: '220px' }}
        >
          {data.map(d => (
            <Option key={d.value}>
              {d.text}
            </Option>
          ))}
        </Select>
      );
    }
}

AirportSearch.propTypes = {
  placeHolder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default AirportSearch;
