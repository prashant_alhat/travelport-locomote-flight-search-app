import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'antd';
import moment from 'moment';

class SearchItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showFlightDetails: false
    };
  }

    showFlightDetails = () => {
      this.setState({
        showFlightDetails: !this.state.showFlightDetails
      });
    }

    renderAirlineLogo = (airlineCode, airlineName) => {
      const logo = require(`../assets/images/${airlineCode}.png`);
      return <img src={logo} alt={airlineName} className="airline-logo" />;
    }

    render() {
      const flighDetails = this.props.searchResult;
      return (
        <div>
          <Row gutter={16}>
            <Col span={8}>
              {this.renderAirlineLogo(flighDetails.Airline.Code, flighDetails.Airline.Name)}
            </Col>
            <Col span={8}>
              <div>
                {flighDetails.Duration}
              </div>
            </Col>
            <Col span={6}>
              {flighDetails.FormattedPrice}
            </Col>
            <Col span={2}>
              <Button
                type="primary"
                icon={this.state.showFlightDetails ? 'up-square-o' : 'down-square-o'}
                onClick={this.showFlightDetails}
                title={this.state.showFlightDetails ? 'Hide Details' : 'Show Details'}
              />
            </Col>
          </Row>
          <Row className="flight-details" style={{ display: this.state.showFlightDetails ? 'block' : 'none' }}>
            <Col span={24}>
              <Row>
                <Col span={6}>
                  <span>
                                    Flight Number:
                  </span>
                </Col>
                <Col span={18}>
                  {flighDetails.FlightNumber}
                </Col>
              </Row>
              <Row>
                <Col span={6}>
                  <span>
                                    Departs:
                  </span>
                </Col>
                <Col span={18}>
                  {moment(flighDetails.Start.DateTime).format('LLLL')}
                  {' '}
                  {flighDetails.Start.CityName}
                  {' '}
                                -
                  {' '}
                  {flighDetails.Start.AirportName}
                </Col>
              </Row>
              <Row>
                <Col span={6}>
                  <span>
                                    Arrives:
                  </span>
                </Col>
                <Col span={18}>
                  {moment(flighDetails.Finish.DateTime).format('LLLL')}
                  {' '}
                  {flighDetails.Finish.CityName}
                  {' '}
                                -
                  {' '}
                  {flighDetails.Finish.AirportName}
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      );
    }
}

SearchItem.propTypes = {
  searchResult: PropTypes.object.isRequired
};

export default SearchItem;
