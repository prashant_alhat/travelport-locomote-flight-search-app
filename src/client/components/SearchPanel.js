import React from 'react';
import PropTypes from 'prop-types';
import {
  Form, Button, DatePicker, Icon, Spin
} from 'antd';
import moment from 'moment';
import AirportSearch from './AirportSearch';
import './flightsearch.scss';

const FormItem = Form.Item;

const dateFormat = 'DD/MM/YYYY';
const defaultFromDate = moment();

class SearchPanel extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err) => {
      if (!err) {
        this.props.searchFlights();
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="flight-search-form">
        <Form onSubmit={this.handleSubmit}>
          <div className="flight-search-form-row">
            <FormItem>
              {getFieldDecorator('fromAirport', {
                initialValue: '',
                rules: [{ required: true, message: "Please select 'FROM' airport" }],
              })(
                <AirportSearch placeHolder="From" onChange={this.props.onFromAirportChanged} />
              )}
            </FormItem>
            <Icon type="arrow-right" className="flight-search-form-arrow" size="large" />
            <FormItem>
              {getFieldDecorator('toAirport', {
                initialValue: '',
                rules: [{ required: true, message: "Please select 'TO' airport" }],
              })(
                <AirportSearch placeHolder="To" onChange={this.props.onToAirportChanged} />
              )}
            </FormItem>
          </div>
          <div className="flight-search-form-row">
            <FormItem style={{ display: 'flex' }}>
              {getFieldDecorator('travelDate', {
                initialValue: null,
                rules: [{ required: true, message: 'Please select travel date' }]
              })(
                <DatePicker
                  initialValue={defaultFromDate}
                  format={dateFormat}
                  onCalendarChange={this.props.onCalendarChange}
                />
              )}

              <Button type="primary" icon="search" className="flight-search-form-btn"
                htmlType="submit" disabled={this.props.fetchingResults}>
                Search
              </Button>
              <Button type="primary" icon="search" className="flight-search-form-btn-responsive"
                htmlType="submit" disabled={this.props.fetchingResults} loading={this.props.fetchingResults}>
                Search
              </Button>
              {
                this.props.fetchingResults
                  ? <Spin className="flight-search-form-spinner" size="large" />
                  : null
              }

            </FormItem>
          </div>
        </Form>
      </div>
    );
  }
}

SearchPanel.propTypes = {
  searchFlights: PropTypes.func.isRequired,
  onFromAirportChanged: PropTypes.func.isRequired,
  onToAirportChanged: PropTypes.func.isRequired,
  onCalendarChange: PropTypes.func.isRequired,
  fetchingResults: PropTypes.bool.isRequired,
};
const SearchPanelForm = Form.create()(SearchPanel);

export default SearchPanelForm;
