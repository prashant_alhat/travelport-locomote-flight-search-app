import React from 'react';
import { Select } from 'antd';

const Option = Select.Option;

class Sort extends React.Component {
  constructor(props) {
    super(props);
  }

    handleChange = (value) => {
      console.log(value);
    }

    render() {
      return (
        <div>
          <Select defaultValue="price" style={{ width: 120 }} onChange={this.handleChange}>
            <Option value="price">
                        Price
            </Option>
            <Option value="duration">
                        Duration
            </Option>
          </Select>

          <Select defaultValue="asc" style={{ width: 120 }} onChange={this.handleChange}>
            <Option value="asc">
                        Low to High
            </Option>
            <Option value="desc">
                        Hight to Low
            </Option>
          </Select>
        </div>
      );
    }
}

export default Sort;
