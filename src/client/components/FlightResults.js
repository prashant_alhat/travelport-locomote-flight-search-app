import React from 'react';
import PropTypes from 'prop-types';
import SeatchItem from './SearchItem';

const FlightResults = props => (
  <div className="flight-result">
    {
      props.searchResults
        .map((searchResult, index) => <SeatchItem searchResult={searchResult} key={index} />)
    }
  </div>);

FlightResults.propTypes = {
  searchResults: PropTypes.array
};

FlightResults.defaultProps = {
  searchResults: []
};

export default FlightResults;
