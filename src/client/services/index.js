import 'babel-polyfill';

module.exports = {
  fetchFlights: async (flightDate, fromAirport, toAirport, sortBy,sortDirection) => {
    let data;
    try {
      const response = await fetch(`/api/flight_search/${flightDate}/${fromAirport}/${toAirport}/${sortBy}/${sortDirection}`);
      data = await response.json();
    } catch (e) {
      console.error(e);
    }
    return data;
  },

  fetchAirports: async (searchQuery) => {
    let data;
    try {
      const response = await fetch(`/api/airports/${searchQuery}`);
      const body = await response.json();
      data = body ? body.map(airport => ({
        text: `${airport.airportName} - ${airport.cityName} - ${airport.countryName}`,
        value: airport.airportCode,
      })) : [];
    } catch (e) {
      console.log(e);
    }
    return data;
  }
};
