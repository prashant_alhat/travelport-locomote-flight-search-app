import React, { Component } from "react";
import { Row, Col, Radio } from "antd";

import "./app.scss";
import "antd/dist/antd.css"

import FlightSearch from "./containers/FlightSearch";

const RadioGroup = Radio.Group;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { showReturnFlightSearch: false };
  }

  onChange = (e) => {
    if (e.target.value === 2) {
      this.setState({
        showReturnFlightSearch: true
      });
    } else {
      this.setState({
        showReturnFlightSearch: false
      });
    }
  }

  render() {
    return (
      <div className="container">
        <Row>
          <Col span={12} className="journey-options">
            <RadioGroup onChange={this.onChange} value={this.state.showReturnFlightSearch ? 2 : 1}>
              <Radio value={1}>One-Way</Radio>
              <Radio value={2}>Return</Radio>
            </RadioGroup>
          </Col>
        </Row>
        <Row>
          <Col span={12} className="flight-search-container">
            <FlightSearch />
          </Col>
          <Col span={12} className="flight-search-container">
            {
              this.state.showReturnFlightSearch ? <FlightSearch /> : null
            }
          </Col>
        </Row>
      </div>
    );
  }
}
