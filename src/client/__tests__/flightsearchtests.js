import React from 'react';
import { shallow, mount } from 'enzyme';

import App from '../App';
import SearchItem from '../components/SearchItem';
import AirportSearch from '../components/AirportSearch';
import SearchPanel from '../components/SearchPanel';

const message = "this is test";
let wrapper;
let container;

const flightData = [
    {
        Airline: { Name: 'Qantas', Code: 'QF' },
        Start:
        {
            DateTime: '2018-08-02T12:30:00+10:00',
            AirportCode: 'MEL',
            AirportName: 'Tullamarine Arpt',
            CityName: 'Melbourne',
            CityCode: 'MEL'
        },
        Finish:
        {
            DateTime: '2018-08-02T19:22:00-04:00',
            AirportCode: 'JFK',
            AirportName: 'John F Kennedy Intl',
            CityName: 'New York',
            CityCode: 'NYC'
        },
        FlightNumber: 567,
        Duration: '20 hr 52 mins',
        Distance: 16695,
        FormattedPrice: '9423.39 AUD',
        Price: 9423.39
    },
    {
        Airline: { Name: 'Qantas', Code: 'QF' },
        Start:
        {
            DateTime: '2018-08-02T23:59:00+10:00',
            AirportCode: 'MEL',
            AirportName: 'Tullamarine Arpt',
            CityName: 'Melbourne',
            CityCode: 'MEL'
        },
        Finish:
        {
            DateTime: '2018-08-03T06:51:00-04:00',
            AirportCode: 'JFK',
            AirportName: 'John F Kennedy Intl',
            CityName: 'New York',
            CityCode: 'NYC'
        },
        FlightNumber: 368,
        Duration: '20 hr 52 mins',
        Distance: 16695,
        FormattedPrice: '9698.72 AUD',
        Price: 9698.72
    }];


/**
 * Difference between shallow and mount is, shallow is used to render an individual component and wont render any child components
 * mount will render complete component tree
 */

it('APP renders without crashing', () => {
    mount(<App />);
});

it('SearchItem rendered correctly', () => {
    const wrapper = mount(<SearchItem searchResult={flightData[0]} />);
    expect(wrapper.find('.ant-btn-primary').exists()).toEqual(true);
    expect(wrapper.find('.flight-details').exists()).toEqual(true);
})

it('SearchItem rendered correctly', () => {
    const wrapper = mount(<SearchItem searchResult={flightData[0]} />);
    expect(wrapper.find('.ant-btn-primary').exists()).toEqual(true);
    expect(wrapper.find('.flight-details').exists()).toEqual(true);
})

it('AirportSearch rendered correctly',() => {
    const wrapper = mount(<AirportSearch placeHolder={message} onChange={()=> {}}/>);
    expect(wrapper.find('.ant-select-search__field').exists()).toEqual(true);
    expect(wrapper.find('.ant-select-selection__placeholder').text()).toEqual(message); //placeholder is set correctly
})

it('SearchPanel rendered correctly',() => {
    const emptyFunction = ()=> {};
    const wrapper = mount(
    <SearchPanel 
        placeHolder={message} 
        searchFlights={emptyFunction} 
        onFromAirportChanged={emptyFunction}
        onToAirportChanged={emptyFunction}
        onCalendarChange={emptyFunction}
        fetchingResults={false}
    />);
    expect(wrapper.find('.flight-search-form-btn').exists()).toEqual(true);
    expect(wrapper.find('.flight-search-form-spinner').exists()).toEqual(false); //fetchingResults is false should not show spinner
})
